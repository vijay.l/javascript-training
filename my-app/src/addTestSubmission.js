import React from "react";
import { ApolloProvider } from "react-apollo";
import { Mutation } from "react-apollo";
import Layout from "./components/layout";
import Client from "./client";
import { Alert } from "rsuite";
import "rsuite/dist/styles/rsuite.min.css";
import { TestEvaluatorList } from "./components/lists";
import { CandidateGql, TestEntryGql } from "./components/gqlStringList";
import AddTestSubmissionForm from "./components/addTestSubmissionForm";
import CheckCandidateForm from "./components/checkCandidateForm";

class AddTestSubmission extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id: ``,
      testEvaluatorId: ``,
      answers: [],
      candidateUsn: null,
      candidate: [],
      disable: true,
      testEvaluator: ``
    };
  }

  handleSelect = eventKey => {
    this.setState({
      testEvaluator: eventKey,
      testEvaluatorId: TestEvaluatorList[eventKey]
    });
  };

  handleChangeAnswers = (stateValue, stateName) => {
    this.setState(state => {
      state.answers[stateName] = stateValue;
    });
  };

  handleChange = (stateValue, stateName) => {
    this.setState({
      [stateName]: stateValue
    });
  };

  getCandidateData = () => {
    const usnVal = this.state.candidateUsn;
    Client.query({
      query: CandidateGql,
      variables: { usn: usnVal }
    })
      .then(result => {
        if (result.data.candidate[0]) {
          this.setState({
            candidate: result.data.candidate[0],
            disable: false
          });
        } else {
          //Delay of 5000ms taken universally
          Alert.error("Enter Valid USN", 5000);
          this.setState({
            candidate: null,
            disable: true
          });
        }
      })
      .catch(error => {
        Alert.error("Candidate doesn't exist" + error, 5000);
      });
  };

  render() {
    return (
      <ApolloProvider client={Client}>
        <Layout>
          <CheckCandidateForm
            handleChange={this.handleChange}
            getCandidateData={this.getCandidateData}
            candidate={this.state.candidate}
          />
          <Mutation
            mutation={TestEntryGql}
            onCompleted={data => {
              Alert.success("Successful!");
            }}
            onError={error => {
              if (error.message.startsWith("GraphQL"))
                return Alert.error("Data enterred is a duplicate entry", 5000);
              if (error.message.startsWith("Server"))
                return Alert.error("Server Error", 5000);
              else return Alert.error("Error! Try Again :(", 5000);
            }}
          >
            {insertTestSubmission => (
              <AddTestSubmissionForm
                renderForm={this.renderForm}
                handleSelect={this.handleSelect}
                answers={this.state.answers}
                testEvaluator={this.state.testEvaluator}
                candidate={this.state.candidate}
                testEvaluatorId={this.state.testEvaluatorId}
                disable={this.state.disable}
                insertTestSubmission={insertTestSubmission}
                handleChangeAnswers={this.handleChangeAnswers}
              />
            )}
          </Mutation>
        </Layout>
      </ApolloProvider>
    );
  }
}

export default AddTestSubmission;
