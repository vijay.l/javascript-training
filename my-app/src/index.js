import React from "react";
import "./index.css";
import App from "./homePage";
import * as serviceWorker from "./serviceWorker";
import { render } from "react-dom";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import AddUser from "./addUser";
import AddTestSubmission from "./addTestSubmission";

render(
  <Router>
    <Switch>
      <Route exact path="/" component={App} />
      <Route path="/add-user" component={AddUser} />
      <Route path="/add-test-submission" component={AddTestSubmission} />
    </Switch>
  </Router>,
  document.getElementById("root")
);

serviceWorker.unregister();
