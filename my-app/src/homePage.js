import React from "react";
import "./App.css";
import { ApolloProvider } from "react-apollo";
import Client from "./client";
import { graphql } from "react-apollo";
import Layout from "./components/layout";
import { Button, ButtonToolbar } from "rsuite";
import "rsuite/dist/styles/rsuite.min.css";
import ReactTable from "react-table";
import "react-table/react-table.css";
import { ThemeColor, SecondaryThemeColor } from "./components/layout";
import { AllCandidatesGql } from "./components/gqlStringList";
import CandidateTableColumns, {
  Columns
} from "./components/candidateTableColumns";

class Homepage extends React.Component {
  onRowClick = rowId => {
    this.setState({ rowNo: rowId });
  };

  render() {
    const CandidatesData = graphql(AllCandidatesGql, {
      props: ({ data: { loading, candidate } }) => ({
        loading,
        candidate
      })
    })(CandidateList);

    function CandidateList({ loading, candidate }) {
      if (loading) {
        return <div>Loading</div>;
      } else if (candidate) {
        return (
          <div className="App">
            <ReactTable
              className="-striped -highlight"
              data={candidate}
              columns={Columns}
              style={{
                margin: "20px",
                overflow: "visible",
                borderRadius: "10px",
                backgroundColor: `white`
              }}
            />
          </div>
        );
      } else {
        return <div>Network Error :(</div>;
      }
    }

    return (
      <ApolloProvider client={Client}>
        <CandidateTableColumns />
        <Layout>
          <div className="App-content">
            <ButtonToolbar style={{ margin: `20px`, paddingBottom: "20px" }}>
              <Button
                color="blue"
                appearance="primary"
                className="App-button"
                style={{
                  backgroundColor: ThemeColor,
                  color: SecondaryThemeColor
                }}
                onClick={() => {
                  this.props.history.push("/add-user");
                }}
              >
                Add User
              </Button>
              <Button
                color="blue"
                appearance="primary"
                style={{
                  backgroundColor: ThemeColor,
                  color: SecondaryThemeColor
                }}
                onClick={() => {
                  this.props.history.push(`/add-test-submission`);
                }}
              >
                Add Test Submission
              </Button>
            </ButtonToolbar>
            <CandidatesData />
          </div>
        </Layout>
      </ApolloProvider>
    );
  }
}

export default Homepage;
