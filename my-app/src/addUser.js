import React from "react";
import Layout from "./components/layout";
import "./App.css";
import { Mutation } from "react-apollo";
import { ApolloProvider } from "react-apollo";
import Client from "./client";
import "rsuite/dist/styles/rsuite.min.css";
import { Alert } from "rsuite";
import { AddCandidateGql } from "./components/gqlStringList";
import AddUserForm from "./components/addUserForm";

class AddUser extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      candidate: []
    };
  }

  handleChange = (eventValue, eventName) => {
    this.setState(state => {
      state.candidate[eventName] = eventValue;
    });
  };

  handleSelect = (eventKey, stateName) => {
    this.setState({ [stateName]: eventKey });
  };

  render() {
    return (
      <ApolloProvider client={Client}>
        <Layout>
          <Mutation
            mutation={AddCandidateGql}
            onCompleted={data => {
              Alert.success("Successful!");
            }}
            onError={error => {
              if (error.message.startsWith("GraphQL"))
                //Delay of 5000ms taken universally
                return Alert.error("Data enterred is a duplicate entry", 5000);
              if (error.message.startsWith("Server"))
                return Alert.error("Server Error", 5000);
              else return Alert.error("Error! Try Again :(", 5000);
            }}
          >
            {(insertCandidate, { loading, error }) => (
              <AddUserForm
                handleChange={this.handleChange}
                handleSelect={this.handleSelect}
                insertCandidate={insertCandidate}
                candidate={this.state.candidate}
              />
            )}
          </Mutation>
        </Layout>
      </ApolloProvider>
    );
  }
}

export default AddUser;
