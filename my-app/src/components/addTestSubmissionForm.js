import React from "react";
import { SecondaryThemeColor, ThemeColor } from "./layout";
import { Form, FormGroup, ControlLabel, Alert } from "rsuite";
import { Button, ButtonToolbar, RadioGroup, Radio, IconButton } from "rsuite";
import { Dropdown, Icon } from "rsuite";
import "rsuite/dist/styles/rsuite.min.css";

class AddTestSubmissionForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      candidate: this.props.candidate,
      answers: this.props.answers
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      candidate: nextProps.candidate,
      answers: nextProps.answers
    });
  }

  renderFormRadioGroups = () => {
    const indices = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];
    return indices.map(index => (
      <FormGroup key={index}>
        <ControlLabel>Answer {index}</ControlLabel>
        <RadioGroup
          name={"answer" + String(index)}
          inline
          onChange={(value, event) => {
            let stateName = "answer" + String(index);
            this.props.handleChangeAnswers(value, stateName);
          }}
        >
          <Radio
            disabled={this.props.disable}
            name="true"
            value="true"
            className="App-radio"
          >
            true
          </Radio>
          <Radio disabled={this.props.disable} name="false" value="false">
            false
          </Radio>
          <Radio disabled={this.props.disable} name="" value={null}>
            not answered
          </Radio>
        </RadioGroup>
      </FormGroup>
    ));
  };

  render() {
    return (
      <div id="test_form" className="App-label-div">
        <Form
          fluid
          onSubmit={e => {
            !this.props.testEvaluator && Alert.error("Enter Test Evaluator!");
            e.preventDefault();
            this.props.testEvaluator &&
              this.props.insertTestSubmission({
                variables: {
                  candidateId: this.props.candidate.id,
                  testEvaluatorId: this.props.testEvaluatorId,
                  answer1: this.state.answers.answer1
                    ? this.state.answers.answer1
                    : null,
                  answer2: this.state.answers.answer2
                    ? this.state.answers.answer2
                    : null,
                  answer3: this.state.answers.answer3
                    ? this.state.answers.answer3
                    : null,
                  answer4: this.state.answers.answer4
                    ? this.state.answers.answer4
                    : null,
                  answer5: this.state.answers.answer5
                    ? this.state.answers.answer5
                    : null,
                  answer6: this.state.answers.answer6
                    ? this.state.answers.answer6
                    : null,
                  answer7: this.state.answers.answer7
                    ? this.state.answers.answer7
                    : null,
                  answer8: this.state.answers.answer8
                    ? this.state.answers.answer8
                    : null,
                  answer9: this.state.answers.answer9
                    ? this.state.answers.answer9
                    : null,
                  answer11: this.state.answers.answer11
                    ? this.state.answers.answer11
                    : null,
                  answer12: this.state.answers.answer12
                    ? this.state.answers.answer12
                    : null,
                  answer13: this.state.answers.answer13
                    ? this.state.answers.answer13
                    : null,
                  answer10: this.state.answers.answer10
                    ? this.state.answers.answer10
                    : null,
                  answer14: this.state.answers.answer14
                    ? this.state.answers.answer14
                    : null,
                  answer15: this.state.answers.answer15
                    ? this.state.answers.answer15
                    : null
                }
              });
          }}
          formValue={this.props.value}
        >
          <FormGroup>
            <ControlLabel>Test Evaluator *</ControlLabel>
            {/*AddDropdown*/}
            <Dropdown
              style={{ backgroundColor: `white`, borderRadius: `8px` }}
              disabled={this.props.disable}
              title={
                this.props.testEvaluator ? this.props.testEvaluator : "Empty"
              }
              onSelect={eventKey => {
                this.props.handleSelect(eventKey);
              }}
              renderTitle={children => {
                return (
                  <IconButton
                    disabled={this.props.disable}
                    appearance="default"
                    icon={<Icon icon="angle-double-down" />}
                    placement="right"
                  >
                    {children}
                  </IconButton>
                );
              }}
            >
              <Dropdown.Item eventKey="Gopal">Gopal</Dropdown.Item>
              <Dropdown.Item eventKey="Chetan">Chetan</Dropdown.Item>
              <Dropdown.Item eventKey="Mayur">Mayur</Dropdown.Item>
              <Dropdown.Item eventKey="Sahil">Sahil</Dropdown.Item>
            </Dropdown>
          </FormGroup>
          {this.renderFormRadioGroups()}
          <FormGroup>
            <ButtonToolbar>
              <Button
                disabled={this.props.disable}
                type="submit"
                appearance="primary"
                color="blue"
                className="App-button"
                style={{
                  backgroundColor: ThemeColor,
                  color: SecondaryThemeColor
                }}
              >
                Submit
              </Button>
            </ButtonToolbar>
          </FormGroup>
        </Form>
      </div>
    );
  }
}

export default AddTestSubmissionForm;
