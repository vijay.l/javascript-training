import { gql } from "apollo-boost";

export const CandidateGql = gql`
  query candidate($usn: String) {
    candidate(where: { usn: { _eq: $usn } }, limit: 1) {
      id
      name
      usn
      gender
      email
      phone_number
      department
    }
  }
`;

export const TestEntryGql = gql`
  mutation insertTestSubmission(
    $candidateId: Int
    $testEvaluatorId: Int
    $answer1: Boolean
    $answer2: Boolean
    $answer3: Boolean
    $answer4: Boolean
    $answer5: Boolean
    $answer6: Boolean
    $answer7: Boolean
    $answer8: Boolean
    $answer9: Boolean
    $answer10: Boolean
    $answer11: Boolean
    $answer12: Boolean
    $answer13: Boolean
    $answer14: Boolean
    $answer15: Boolean
  ) {
    insert_test_submission(
      objects: [
        {
          candidate_id: $candidateId
          test_evaluator_id: $testEvaluatorId
          answer_1: $answer1
          answer_2: $answer2
          answer_3: $answer3
          answer_4: $answer4
          answer_5: $answer5
          answer_6: $answer6
          answer_7: $answer7
          answer_8: $answer8
          answer_9: $answer9
          answer_10: $answer10
          answer_11: $answer11
          answer_12: $answer12
          answer_13: $answer13
          answer_14: $answer14
          answer_15: $answer15
        }
      ]
    ) {
      returning {
        candidate_id
        test_evaluator_id
      }
    }
  }
`;

export const AddCandidateGql = gql`
  mutation insertCandidate(
    $name: String
    $usn: String
    $email: String
    $gender: gender
    $phoneNumber: String
    $department: String
    $questionSet: Int
  ) {
    insert_candidate(
      objects: [
        {
          name: $name
          usn: $usn
          email: $email
          gender: $gender
          phone_number: $phoneNumber
          department: $department
          question_set_id: $questionSet
        }
      ]
    ) {
      returning {
        name
        id
      }
    }
  }
`;

export const AllCandidatesGql = gql`
  query candidate {
    candidate {
      id
      usn
      name
      gender
      email
      phone_number
      department
      question_set_id
    }
  }
`;

export const CorrectAnsCountGql = gql`
  query testSubmission($candidateId: Int!) {
    test_submission(where: { candidate_id: { _eq: $candidateId } }) {
      id
      candidate_id
      test_evaluator_id
      answer_1
      answer_2
      answer_3
      answer_4
      answer_5
      answer_6
      answer_7
      answer_8
      answer_9
      answer_10
      answer_11
      answer_12
      answer_13
      answer_14
      answer_15
    }
  }
`;
