import React from "react";
import Client from "../client";
import { CorrectAnsCountGql } from "./gqlStringList";
import { Alert } from "rsuite";

class GetCorrectAnswerCount extends React.Component {
  constructor(props) {
    super(props);
    this.state = { correctCount: 0 };
  }

  showTestDetails = rowNo => {
    Client.query({
      query: CorrectAnsCountGql,
      variables: { candidateId: rowNo }
    }).then(result => {
      return result.data.test_submission.map(
        ({
          id,
          candidate_id,
          test_evaluator_id,
          answer_1,
          answer_2,
          answer_3,
          answer_4,
          answer_5,
          answer_6,
          answer_7,
          answer_8,
          answer_9,
          answer_10,
          answer_11,
          answer_12,
          answer_13,
          answer_14,
          answer_15
        }) => {
          if (answer_1) {
            this.setState({ correctCount: this.state.correctCount + 1 });
          }
          if (answer_2) {
            this.setState({ correctCount: this.state.correctCount + 1 });
          }
          if (answer_3) {
            this.setState({ correctCount: this.state.correctCount + 1 });
          }
          if (answer_4) {
            this.setState({ correctCount: this.state.correctCount + 1 });
          }
          if (answer_5) {
            this.setState({ correctCount: this.state.correctCount + 1 });
          }
          if (answer_6) {
            this.setState({ correctCount: this.state.correctCount + 1 });
          }
          if (answer_7) {
            this.setState({ correctCount: this.state.correctCount + 1 });
          }
          if (answer_8) {
            this.setState({ correctCount: this.state.correctCount + 1 });
          }
          if (answer_9) {
            this.setState({ correctCount: this.state.correctCount + 1 });
          }
          if (answer_10) {
            this.setState({ correctCount: this.state.correctCount + 1 });
          }
          if (answer_11) {
            this.setState({ correctCount: this.state.correctCount + 1 });
          }
          if (answer_12) {
            this.setState({ correctCount: this.state.correctCount + 1 });
          }
          if (answer_13) {
            this.setState({ correctCount: this.state.correctCount + 1 });
          }
          if (answer_14) {
            this.setState({ correctCount: this.state.correctCount + 1 });
          }
          if (answer_15) {
            this.setState({ correctCount: this.state.correctCount + 1 });
          }
          Alert.info(`Successful Attempts: ${this.state.correctCount}`, 5000);
          return this.setState({ correctCount: 0 });
        }
      );
    });
  };

  componentDidMount() {
    this.showTestDetails(this.props.rowNo);
  }

  componentWillReceiveProps(nextProps) {
    this.showTestDetails(nextProps.rowNo);
  }

  render() {
    return <p></p>;
  }
}

export default GetCorrectAnswerCount;
