import React from "react";
import {
  Form,
  FormGroup,
  ControlLabel,
  ButtonToolbar,
  Button,
  Dropdown
} from "rsuite";
import { FormControl, Alert } from "rsuite";
import { QuestionSetList } from "./lists";
import { SecondaryThemeColor } from "./layout";

class AddUserForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = { candidate: this.props.candidate };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ candidate: nextProps.candidate });
  }

  render() {
    return (
      <div
        className="App-label-div"
        style={{ backgroundColor: SecondaryThemeColor }}
      >
        <Form
          fluid
          onSubmit={e => {
            e.preventDefault();
            this.state.candidate.usn &&
              this.state.candidate.phone_number &&
              this.state.candidate.question_set &&
              this.props.insertCandidate({
                variables: {
                  name: this.state.candidate.name,
                  email: this.state.candidate.email,
                  usn: this.state.candidate.usn,
                  gender: this.state.candidate.gender,
                  phoneNumber: this.state.candidate.phone_number,
                  department: this.state.candidate.department,
                  questionSet:
                    QuestionSetList[this.state.candidate.question_set]
                }
              });
            (!this.state.candidate.usn ||
              !this.state.candidate.phone_number ||
              !this.state.candidate.question_set) &&
              Alert.error("One or more mandatory fields empty!");
          }}
          formValue={this.state.candidate.value}
        >
          <FormGroup
            onChange={event => {
              this.props.handleChange(event.target.value, event.target.name);
            }}
          >
            <ControlLabel>Name </ControlLabel>
            <FormControl type="text" name="name" />
          </FormGroup>
          <FormGroup
            onChange={event => {
              this.props.handleChange(event.target.value, event.target.name);
            }}
          >
            <ControlLabel>USN *</ControlLabel>
            <FormControl type="text" name="usn" />
          </FormGroup>
          <FormGroup
            onChange={event => {
              this.props.handleChange(event.target.value, event.target.name);
            }}
          >
            <ControlLabel>Email</ControlLabel>
            <FormControl type="text" name="email" />
          </FormGroup>
          <FormGroup
            onChange={event => {
              this.props.handleChange(event.target.value, event.target.name);
            }}
          >
            <ControlLabel>Phone Number *</ControlLabel>
            <FormControl type="text" name="phone_number" />
          </FormGroup>
          <FormGroup
            onChange={event => {
              this.props.handleChange(event.target.value, event.target.name);
            }}
          >
            <ControlLabel>Department</ControlLabel>
            <FormControl type="text" name="department" />
          </FormGroup>
          <FormGroup>
            <ControlLabel>Question Set *</ControlLabel>
            <Dropdown
              style={{ backgroundColor: "white", borderRadius: "8px" }}
              title={
                this.state.candidate.question_set
                  ? this.state.candidate.question_set
                  : "Empty"
              }
              onSelect={eventKey => {
                this.props.handleSelect(eventKey, "question_set");
                this.setState(state => {
                  state.candidate.question_set = eventKey;
                });
              }}
            >
              <Dropdown.Item eventKey="Set A">Set A</Dropdown.Item>
              <Dropdown.Item eventKey="Set B">Set B</Dropdown.Item>
              <Dropdown.Item eventKey="Set C">Set C</Dropdown.Item>
            </Dropdown>
          </FormGroup>
          <FormGroup>
            <ControlLabel>Gender</ControlLabel>
            <Dropdown
              style={{ backgroundColor: "white", borderRadius: "8px" }}
              title={
                this.state.candidate.gender
                  ? this.state.candidate.gender
                  : "Empty"
              }
              onSelect={eventKey => {
                this.props.handleSelect(eventKey, "gender");
                this.setState(state => {
                  state.candidate.gender = eventKey;
                });
              }}
            >
              <Dropdown.Item eventKey="Male">Male</Dropdown.Item>
              <Dropdown.Item eventKey="Female">Female</Dropdown.Item>
            </Dropdown>
          </FormGroup>
          <FormGroup>
            <ButtonToolbar>
              <Button type="submit" size="lg" appearance="primary" color="blue">
                Submit
              </Button>
            </ButtonToolbar>
          </FormGroup>
        </Form>
      </div>
    );
  }
}

export default AddUserForm;
