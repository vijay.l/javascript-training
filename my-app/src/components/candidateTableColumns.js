import React from "react";
import GetCorrectAnswerCount from "../components/getCorrectAnswerCount";
import { IconButton, Icon } from "rsuite";
import { ThemeColor } from "./layout";

export var Columns = [];
class CandidateTableColumns extends React.Component {
  constructor(props) {
    super(props);
    this.state = { rowNo: null };
  }

  render() {
    Columns = [
      {
        id: "show",
        width: 50,
        Cell: row => (
          <div
            ref={ref => {
              this.container = ref;
            }}
          >
            <IconButton
              circle
              size="sm"
              icon={<Icon icon="info"></Icon>}
              style={{ color: ThemeColor }}
              onClick={rows => {
                this.setState({ rowNo: row.original.id });
              }}
            />
          </div>
        )
      },
      {
        Header: "Sl No",
        accessor: "id",
        maxWidth: 50,
        style: { textAlign: "center", overflow: "visible" }
      },
      {
        Header: "Name",
        accessor: "name",
        width: 200,
        style: { textAlign: "center" }
      },
      {
        Header: "USN",
        accessor: "usn",
        width: 200,
        style: { textAlign: "center" }
      },
      {
        Header: "Gender",
        accessor: "gender",
        width: 100,
        style: { textAlign: "center" }
      },
      {
        Header: "Email",
        accessor: "email",
        width: 150,
        style: { textAlign: "center" }
      },
      {
        Header: "Ph. No.",
        accessor: "phone_number",
        width: 200,
        style: { textAlign: "center" }
      },
      {
        Header: "Department",
        accessor: "department",
        width: 150,
        style: { textAlign: "center" }
      },
      {
        Header: "Q Set",
        accessor: "question_set_id",
        width: 70,
        style: { textAlign: "center" }
      }
    ];

    return <GetCorrectAnswerCount rowNo={this.state.rowNo} />;
  }
}

export default CandidateTableColumns;
