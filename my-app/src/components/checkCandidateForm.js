import React from "react";
import { SecondaryThemeColor, ThemeColor } from "./layout";
import { Form, FormGroup, FormControl, ControlLabel } from "rsuite";
import { Button } from "rsuite";
import "rsuite/dist/styles/rsuite.min.css";

class CheckCandidateForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = { candidate: this.props.candidate };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ candidate: nextProps.candidate });
  }

  render() {
    return (
      <div className="App-test-div">
        <div className="App-usn-form">
          <Form fluid formValue={this.props.value}>
            <FormGroup>
              <ControlLabel>Enter USN: </ControlLabel>
              <FormControl
                name="candidateUsn"
                value={this.state.candidate.usn}
                onChange={event => {
                  this.props.handleChange(event, "candidateUsn");
                }}
                type="text"
              />
            </FormGroup>
            <FormGroup>
              <Button
                onClick={this.props.getCandidateData}
                appearance="primary"
                style={{
                  backgroundColor: ThemeColor,
                  color: SecondaryThemeColor
                }}
              >
                Submit
              </Button>
            </FormGroup>
          </Form>
        </div>
        <div>
          <div>
            <h1>Candidate Details</h1>
            <h3>
              <label>Name: </label>
              {this.state.candidate.name}
            </h3>
            <h3>
              <label>Candidate Id: </label>
              {this.state.candidate.id}
            </h3>
            <h3>
              <label>Branch: </label> {this.state.candidate.department}
            </h3>
          </div>
        </div>
      </div>
    );
  }
}

export default CheckCandidateForm;
