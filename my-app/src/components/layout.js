import React from "react";
import "../App.css";
import { withRouter } from "react-router-dom";

export const ThemeColor = "#003F87";
export const SecondaryThemeColor = "white";

class Layout extends React.Component {
  render() {
    return (
      <div className="App">
        <div className="App-background">
          <h2>
            <a href="/" className="App-link">
              Recruitment Data
            </a>
          </h2>
        </div>
        {this.props.children}
      </div>
    );
  }
}

export default withRouter(Layout);
