CREATE TABLE candidate (
  candidate_id INTEGER PRIMARY KEY NOT NULL,
  usn TEXT ,
  name TEXT ,
  email TEXT ,
  phone_number TEXT ,
  department TEXT ,
  branch TEXT ,
  gender TEXT ,
  question_set_id TEXT REFERENCES question_set(set_id) NOT NULL
);
CREATE TABLE test_submission (
    test_id INTEGER PRIMARY KEY NOT NULL,
    candidate_id INTEGER REFERENCES candidate(candidate_id) NOT NULL,
    test_evaluator_id INTEGER REFERENCES test_evaluator(evaluator_id) NOT NULL,
    answer_1 BOOLEAN ,
    answer_2 BOOLEAN ,
    answer_3 BOOLEAN ,
    answer_4 BOOLEAN ,
    answer_5 BOOLEAN ,
    answer_6 BOOLEAN ,
    answer_7 BOOLEAN ,
    answer_8 BOOLEAN ,
    answer_9 BOOLEAN ,
    answer_10 BOOLEAN ,
    answer_11 BOOLEAN ,
    answer_12 BOOLEAN ,
    answer_13 BOOLEAN ,
    answer_14 BOOLEAN ,
    answer_15 BOOLEAN 
);

CREATE TABLE question_set (
   set_id INTEGER PRIMARY KEY NOT NULL,
   name TEXT 
);

CREATE TABLE test_evaluator(
  evaluator_id INTEGER PRIMARY KEY NOT NULL,
  name TEXT
);
