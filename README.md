# Javascript Training 
`git clone` : Clone the repository  

## Docker
`docker-compose up -d` : Set Up the docker container with specifics using


## Additional Steps
- Duplicate the database as in the file: [recruitment.sql](recruitment.sql)
- Populate the database

## Dependencies
-  React Apollo Client For Graphql
-  React Router for routing and switching
-  React Table used for all tables rendered
-  Rsuite used for all other ui elements

## Running the app
- `yarn install` : to install all the dependencies and set them up
- `yarn start` : to deploy


